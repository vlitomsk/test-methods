package ru.nsu.fit.tests.api;

public interface IHttpMethod {
    String exec(String path, String query);
    String getName();
}
