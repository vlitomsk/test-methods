package ru.nsu.fit.tests.api;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.jvnet.jaxb2_commons.lang.Validate;
import ru.nsu.fit.services.log.Logger;
import ru.nsu.fit.tests.api.IHttpMethod;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class LabJsonService {

    LabJsonService() {
        this("admin", "setup", "http://localhost:8080/endpoint/rest");
    }

    LabJsonService(String username,
                   String password,
                   String baseUrl)
    {
        Validate.notNull(baseUrl);
        this.baseUrl = baseUrl;

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        clientConfig.register(feature) ;
        clientConfig.register(JacksonFeature.class);

        client = ClientBuilder.newClient( clientConfig );
    }

    public String doGet(String path, String qry) { return exec(getMethod, path, qry); }
    public String doPost(String path, String qry) { return exec(postMethod, path, qry); }

    public String exec(IHttpMethod method, String path, String qry) {
        Logger.info(method.getName() + " " + path + " TX:\t" + qry);
        String rx = method.exec(path, qry);
        Logger.info(method.getName() + " " + path + " RX:\t" + rx);
        return rx;
    }

    private Invocation.Builder ibuilder(String path) {
        WebTarget webTarget = client.target(baseUrl).path(path);
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder;
    }

    private String readResponse(Response resp) {
        return resp.readEntity(String.class);
    }

    private class GetMethod implements IHttpMethod {

        @Override
        public String exec(String path, String query) {
            return readResponse(ibuilder(path).post(Entity.entity(query, MediaType.APPLICATION_JSON)));
        }

        @Override
        public String getName() {
            return "GET";
        }
    }

    private class PostMethod implements IHttpMethod {

        @Override
        public String exec(String path, String query) {
            return readResponse(ibuilder(path).post(Entity.entity(query, MediaType.APPLICATION_JSON)));
        }

        @Override
        public String getName() {
            return "POST";
        }
    }

    private String baseUrl;
    private Client client;
    private IHttpMethod getMethod = new GetMethod();
    private IHttpMethod postMethod = new PostMethod();
}
