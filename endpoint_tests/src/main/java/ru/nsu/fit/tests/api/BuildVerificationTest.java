package ru.nsu.fit.tests.api;

import org.testng.annotations.Test;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BuildVerificationTest {
    @Test(description = "Create customer via API.")
    public void createCustomer() {
        jsonSvc.doPost("create_customer", "{\n" +
                "\t\"firstName\":\"Johnds\",\n" +
                "    \"lastName\":\"Weak\",\n" +
                "    \"login\":\"helloworld123@login.com\",\n" +
                "    \"pass\":\"password123\",\n" +
                "    \"balance\":\"100\"\n" +
                "}");
    }

    @Test(description = "Try to log into the system.", dependsOnMethods = "createCustomer")
    public void login() {
        // TODO: попробовать получить данные для созданного на предыдущем шаге customer.
    }

    private LabJsonService jsonSvc = new LabJsonService();
}
