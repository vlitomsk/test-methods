package ru.nsu.fit.tests.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.internal.inject.Custom;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.data.*;
import ru.nsu.fit.services.log.Logger;
import sun.rmi.runtime.Log;

import java.io.IOException;

public class CreateCustomerTest {
    @BeforeClass(description = "")
    public void beforeTests() {
        initialCustomer = new Customer()
                .setFirstName("John")
                .setLastName("Weak")
                .setLogin("helloworld123")
                .setPass("password123")
                .setBalance(0);
    }

    @Test(description = "Initial customers cleanup.")
    public void initialCleanup() {
        String responseText = jsonSvc.doPost("remove_customer/"+ initialCustomer.getLogin(), "");
        try {
            Customer respMessage = mapper.readValue(responseText, Customer.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Assert.assertTrue(false);
    }


    @Test(description = "Create customer via API.", dependsOnMethods = "initialCleanup")
    public void createCustomer()  { //is it okay to make throws here?
        String responseText = null;
        try {
            responseText = jsonSvc.doPost("create_customer", mapper.writeValueAsString(initialCustomer));
            Customer customerAfterCreateMethod = null;
            try {
                customerAfterCreateMethod = mapper.readValue(responseText, Customer.class);
            } catch (IOException e) {
                e.printStackTrace();
                Logger.error(responseText);
                throw new RuntimeException(e);
            }
            customerAfterCreateMethod.setId(initialCustomer.getId());
            Assert.assertTrue(customerAfterCreateMethod.equals(initialCustomer));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    class ResponseMessage {

        private String message;

        public ResponseMessage setMessage(String message) {
            this.message = message;
            return this;
        }

        public String getMessage() {
            return this.message;
        }

    }


    @Test(description = "Create customer via API second time.", dependsOnMethods = "createCustomer")
    public void createCustomer2() {
        String json = null;
        try {
            json = mapper.writeValueAsString(initialCustomer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String responseText = jsonSvc.doPost("create_customer", json);
        Assert.assertTrue(responseText.startsWith( "Customer already exists"));
//        Assert.fail();;
//        Assert.assertEquals("Customer already exists", respMessage.getMessage());
    }

    @Test(description = "Remove created customer.", dependsOnMethods = "createCustomer2")
    public void removeCustomer() {
        String responseText = jsonSvc.doPost("remove_customer/"+ initialCustomer.getLogin(), "");

        try {
            Customer respMessage = mapper.readValue(responseText, Customer.class);
            Assert.assertEquals(initialCustomer.getLogin(), respMessage.getLogin());
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    private LabJsonService jsonSvc = new LabJsonService();
    private ObjectMapper mapper = new ObjectMapper();
    private Customer initialCustomer;
}
