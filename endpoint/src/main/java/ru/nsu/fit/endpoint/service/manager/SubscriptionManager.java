package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.List;
import java.util.UUID;

public class SubscriptionManager extends ParentManager {
    public SubscriptionManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает подписку. Ограничения:
     * 1. Подписки с таким планом пользователь не имеет.
     * 2. Стоймость подписки не превышает текущего баланса кастомера и после покупки вычитается из его баласа.
     */

    void validateSubscription(Subscription s) {
        Validate.notNull(s, "subscription cant be null");
        Validate.notNull(s.getCustomerId(), "subscription customer id cant be null");
        Validate.notNull(s.getPlanId(), "subscription plan id  cant be null");

        Validate.isTrue(dbService.hasEnoughMoney(s.getCustomerId(), s.getPlanId()), "user hasnt enough money");
        boolean alreadyHas = dbService.getPlansByCustomer(s.getCustomerId()).stream().anyMatch(
                plan -> (plan != null && s.getPlanId().equals(plan.getId())));
        Validate.isTrue(!alreadyHas, "user has already subscribed to given plan");
    }

    public Subscription createSubscription(Subscription subscription) {
        validateSubscription(subscription);
        return dbService.createSubscription(subscription);
    }

    public void removeSubscription(UUID subscriptionId) {
        dbService.removeSubscription(subscriptionId);
    }

    /**
     * Возвращает список подписок указанного customer'а.
     */
    public List<Subscription> getSubscriptions(UUID customerId) {
        return dbService.getSubscriptionsByCustomer(customerId);
    }
}
