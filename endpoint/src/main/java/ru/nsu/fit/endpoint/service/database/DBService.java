package ru.nsu.fit.endpoint.service.database;

import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";
    private static final String GET_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String UPDATE_CUSTOMER_NAMES = "UPDATE CUSTOMER SET first_name='%s', last_name='%s' WHERE id='%s'";


    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        init();
    }

    private void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    private <R> List<R> selectAll(String query, Function<ResultSet, R> mapper) {
        List<R> result = new ArrayList<>();
        synchronized (generalMutex) {
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    result.add(mapper.apply(rs));
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    private <R> R selectOne(String query, Function<ResultSet, R> mapper) {
        synchronized (generalMutex) {
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                if (rs.next()) {
                    return (mapper.apply(rs));
                } else {
                    return null;
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private void doUpdate(String query) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    private void doUpdate(String[] trans) {
        try {
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            for (String qry : trans) { statement.executeUpdate(qry); }
            connection.commit();
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static <T> T throwOnNull(T t, String errorMsg) {
        if (null == t) {
            throw new IllegalArgumentException(errorMsg);
        }
        return t;
    }

    private static Customer mapCustomer(ResultSet rs) {
        Customer CUSTOMERData = null;
        try {
            CUSTOMERData = new Customer()
                    .setId(UUID.fromString(rs.getString("id")))
                    .setFirstName(rs.getString("first_name"))
                    .setLastName(rs.getString("last_name"))
                    .setLogin(rs.getString("login"))
                    .setPass(rs.getString("pass"))
                    .setBalance(rs.getInt("balance"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return CUSTOMERData;
    }

    private static Plan mapPlan(ResultSet rs) {
        Plan PLANData = null;
        try {
            PLANData = new Plan()
                    .setId(UUID.fromString(rs.getString("id")))
                    .setName(rs.getString("name"))
                    .setDetails(rs.getString("details"))
                    .setFee(rs.getInt("fee"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return PLANData;
    }

    private static Subscription mapSubscription(ResultSet rs) {
        try {
            return new Subscription()
                    .setId(UUID.fromString(rs.getString("id")))
                    .setCustomerId(UUID.fromString(rs.getString("CUSTOMER_id")))
                    .setPlanId(UUID.fromString(rs.getString("PLAN_id")));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Integer mapFee(ResultSet rs) {
        try {
            return rs.getInt("fee");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Long mapCnt(ResultSet rs) {
        try {
            return rs.getLong("cnt");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static UUID mapId(ResultSet rs) {
        try {
            return UUID.fromString(rs.getString("id"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Customer createCustomer(Customer CUSTOMERData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: '%s'", CUSTOMERData));

            CUSTOMERData.setId(UUID.randomUUID());
            doUpdate(String.format(
                        INSERT_CUSTOMER,
                        CUSTOMERData.getId(),
                        CUSTOMERData.getFirstName(),
                        CUSTOMERData.getLastName(),
                        CUSTOMERData.getLogin(),
                        CUSTOMERData.getPass(),
                        CUSTOMERData.getBalance()));
            return CUSTOMERData;
        }
    }

    public List<Customer> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");
            return selectAll(SELECT_CUSTOMERS, DBService::mapCustomer);
        }
    }

    public UUID getCustomerIdByLogin(String CUSTOMERLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", CUSTOMERLogin));
            return selectOne(String.format(SELECT_CUSTOMER, CUSTOMERLogin), DBService::mapId);
        }
    }

    public Plan createPlan(Plan PLAN) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", PLAN));
            PLAN.setId(UUID.randomUUID());
            doUpdate(String.format(INSERT_PLAN, PLAN.getId(), PLAN.getName(), PLAN.getDetails(), PLAN.getFee()));
            return PLAN;
        }
    }

    public Customer getCustomerById(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerById' was called with data '%s'.", id.toString()));
            return throwOnNull(
                    selectOne(String.format(GET_CUSTOMER_BY_ID, id.toString()), DBService::mapCustomer),
                    "Customer with id '" + id + " was not found");
        }
    }

    public Customer getCustomerByLogin(String login) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerByLogin' was called with data '%s'.", login));
            return throwOnNull(
                    selectOne(String.format("SELECT * FROM CUSTOMER WHERE login='%s'", login), DBService::mapCustomer),
                    "Customer with login '" + login + " was not found");
        }
    }

    public Customer updateCustomerNames(Customer CUSTOMER) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updateCustomerNames' was called with data: '%s'", CUSTOMER));
            doUpdate(String.format(UPDATE_CUSTOMER_NAMES, CUSTOMER.getFirstName(), CUSTOMER.getLastName(), CUSTOMER.getId()));
            return CUSTOMER;
        }
    }

    public void removeCustomerById(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removeCustomerById' was called with data: '%s'", id));
            doUpdate(String.format("DELETE FROM CUSTOMER WHERE id='%s'", id));
        }
    }

    public Customer updateBalance(UUID CUSTOMERId, int amount) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updateBalance' was called with data: '%s', %s", CUSTOMERId, amount));

            doUpdate(String.format("UPDATE CUSTOMER SET balance=balance+%s WHERE id='%s'", amount, CUSTOMERId));
            return throwOnNull(
                    selectOne(String.format(GET_CUSTOMER_BY_ID, CUSTOMERId), DBService::mapCustomer),
                    "Customer with id '" + CUSTOMERId + " was not found");
        }
    }

    public Plan getPlanByName(String name) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getPlanByName' was called with data: '%s", name));
            return selectOne(String.format("SELECT * FROM PLAN WHERE name='%s'", name), DBService::mapPlan);
        }
    }

    public Plan updatePlan(Plan PLAN) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updatePlan' was called with data: '%s", PLAN));
            doUpdate(String.format("UPDATE PLAN SET name='%s', details='%s', fee=%s WHERE id='%s'",
                    PLAN.getName(), PLAN.getDetails(), PLAN.getFee(), PLAN.getId()));
            return PLAN;
        }
    }

    public void removePlan(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removePlan' was called with data: '%s", id));
            doUpdate(String.format("DELETE FROM PLAN WHERE id='%s", id));
        }
    }

    public List<Plan> getPlansByCustomer(UUID CUSTOMERId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getPlansByCustomer' was called with data: '%s", CUSTOMERId));
            return selectAll(String.format("SELECT PLAN.id id, PLAN.name name, PLAN.details details, PLAN.fee fee " +
                    " FROM CUSTOMER, PLAN " +
                    " WHERE CUSTOMER.balance >= PLAN.fee AND CUSTOMER.id='%s'", CUSTOMERId), DBService::mapPlan);
        }
    }

    public List<Plan> getAllPlans() {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getAllPlans' was called"));
            return selectAll("SELECT PLAN.id id, PLAN.name name, PLAN.details details, PLAN.fee fee " +
                    " FROM PLAN ", DBService::mapPlan);
        }
    }

    public boolean hasEnoughMoney(UUID CUSTOMERId, UUID PLANId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'hasEnoughMoney' was called with data: CUSTOMERId='%s' PLANId='%s'", CUSTOMERId, PLANId));
            return selectOne(String.format("SELECT count(*) cnt " +
                    " FROM CUSTOMER c " +
                    "   INNER JOIN SUBSCRIPTION s ON s.CUSTOMER_id=c.id " +
                    "   INNER JOIN PLAN p ON s.PLAN_id=p.id " +
                    " WHERE c.id='%s' AND p.id='%s'", CUSTOMERId, PLANId), DBService::mapCnt) != null;
        }
    }

    public Subscription createSubscription(Subscription SUBSCRIPTION) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createSubscription' was called with data: '%s'", SUBSCRIPTION));
            SUBSCRIPTION.setId(UUID.randomUUID());
            Integer PLANFee = selectOne(String.format("SELECT fee FROM PLAN WHERE id='%s'", SUBSCRIPTION.getPlanId()), DBService::mapFee);
            if (null == PLANFee) { // select failed
                throw new IllegalArgumentException("cant find CUSTOMER with given id");
            }
            doUpdate(new String[]{
                String.format("INSERT INTO SUBSCRIPTION(id, CUSTOMER_id, PLAN_id)" +
                        "VALUES ('%s', '%s', '%s')", SUBSCRIPTION.getId(), SUBSCRIPTION.getCustomerId(), SUBSCRIPTION.getPlanId()),
                String.format("UPDATE CUSTOMER SET balance=balance-%s WHERE id='%s'", PLANFee, SUBSCRIPTION.getCustomerId())
            });
            return SUBSCRIPTION;
        }
    }

    public List<Subscription> getSubscriptionsByCustomer(UUID CUSTOMERId) {
        synchronized (generalMutex) {
            logger.info("Method 'getSubscriptionsByCustomer' was called with CUSTOMERId='%s'", CUSTOMERId);
            return selectAll(String.format("SELECT SUBSCRIPTION.id id, SUBSCRIPTION.CUSTOMER_id CUSTOMER_id, SUBSCRIPTION.PLAN_id PLAN_id " +
                    " FROM SUBSCRIPTION s INNER JOIN CUSTOMER c ON s.CUSTOMER_id=c.id " +
                    " WHERE c.id='%s'", CUSTOMERId), DBService::mapSubscription);
        }
    }

    public void removeSubscription(UUID SUBSCRIPTIONId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removeSubscription' was called with data: '%s", SUBSCRIPTIONId));
            doUpdate(String.format("DELETE FROM SUBSCRIPTION WHERE id='%s", SUBSCRIPTIONId));
        }
    }
}
