package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */

    private boolean containsIcase(String haystack, String needle) {
        return haystack.toLowerCase().contains(needle.toLowerCase());
    }

    private boolean hasTick(String s) {
        return s.contains("'");
    }

    private void validateFirstName(String firstName) {
        Validate.isTrue(!hasTick(firstName), "firstname can't contain `'` symbol");
        Validate.isTrue(firstName.trim().length() >= 2 && firstName.trim().length() <= 12, "First name: invalid length.");
    }

    private void validateLastName(String lastName) {
        Validate.isTrue(!hasTick(lastName), "lastname can't contain `'` symbol");
        Validate.isTrue(lastName.trim().length() >= 2 && lastName.trim().length() <= 12, "Last name: invalid length.");
    }

    public Customer createCustomer(Customer customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");
        Validate.notNull(customer.getPass());
        Validate.notNull(customer.getLogin());
        Validate.notNull(customer.getFirstName());
        Validate.notNull(customer.getLastName());

        customer.setFirstName(customer.getFirstName().trim());
        customer.setLastName(customer.getLastName().trim());
        customer.setLogin(customer.getLogin().toLowerCase().trim());

        Validate.isTrue(!hasTick(customer.getLogin()), "login can't contain `'` symbol");
        Validate.isTrue(!hasTick(customer.getPass()), "pwd can't contain `'` symbol");

        Validate.isTrue(customer.getPass().length() >= 6 && customer.getPass().length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("123qwe"), "Password is easy.");

        Validate.isTrue(!customer.getPass().contains(customer.getLogin()), "Password can't contain login as substring");
        Validate.isTrue(!containsIcase(customer.getPass(), customer.getFirstName()), "Password can't contain first name as substring");
        Validate.isTrue(!containsIcase(customer.getPass(), customer.getLastName()), "Password can't contain last name as substring");

        validateFirstName(customer.getFirstName());;
        validateLastName(customer.getLastName());
        Validate.isTrue(customer.getBalance() == 0, "Initial customer cant have non-zero balance");

        Validate.isTrue(dbService.getCustomerIdByLogin(customer.getLogin()) == null, "Customer already exists");

        return dbService.createCustomer(customer);
    }

    public Customer getByLogin(String login) {
        Validate.isTrue(!hasTick(login));
        return dbService.getCustomerByLogin(login.toLowerCase());
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<Customer> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        Customer current = dbService.getCustomerById(customer.getId());
        Validate.notNull(current, "Given customer doesnt exist");
        Validate.isTrue(customer.getLogin().equalsIgnoreCase(current.getLogin()), "mismatched customers (login)");
        Validate.isTrue(customer.getBalance() == current.getBalance(), "mismatched customers (balance)");
        Validate.isTrue(current.getPass() == null || customer.getPass() == null || customer.getPass().equals(current.getPass()),
                "mismatched customers (passwd)");

        validateFirstName(customer.getFirstName());
        validateLastName(customer.getLastName());

        return dbService.updateCustomerNames(customer);
    }

    public void removeCustomer(UUID id) {
        Validate.notNull(id, "Id can't be null");
        dbService.removeCustomerById(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public Customer topUpBalance(UUID customerId, int amount) {
        Validate.isTrue(amount > 0, "Can't add non-positive amount");
        return dbService.updateBalance(customerId, amount);
    }
}
