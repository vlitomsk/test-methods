package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.List;
import java.util.UUID;
import java.util.function.IntPredicate;

public class PlanManager extends ParentManager {
    public PlanManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */

    void validatePlan(Plan plan) {
        Validate.notNull(plan, "Plan can't be null");
        Validate.notNull(plan.getName(), "Plan name can't be null");
        Validate.notNull(plan.getDetails(), "Plan details can't be null");

        plan.setName(plan.getName().trim());
        plan.setDetails(plan.getDetails().trim());

        Validate.isTrue(plan.getName().length() >= 2 && plan.getName().length() <= 128, "Invalid plan name length (2..128)");

        String punct = ".,:;?\"";
        IntPredicate predValidChars = (c) -> Character.isAlphabetic(c) || Character.isWhitespace(c) || punct.indexOf(c) != -1;

        Validate.isTrue(plan.getName().chars().allMatch(predValidChars), "Plan name contains invalid chars. Punctuation available: " + punct);
        Validate.isTrue(plan.getDetails().chars().allMatch(predValidChars), "Plan details contains invalid chars. Punctuation available: " + punct);
        Validate.isTrue(plan.getDetails().length() >= 1 && plan.getDetails().length() <= 1024 , "Invalid plan details length (1..1024)");
        Validate.isTrue(plan.getFee() >= 0 && plan.getFee() <= 999999, "Invalid plan fee (must be 0..999999)");

        Validate.isTrue (dbService.getPlanByName(plan.getName()) == null, "Plan with given name exists");
    }

    public Plan createPlan(Plan plan) {
        validatePlan(plan);
        return dbService.createPlan(plan);
    }

    public Plan updatePlan(Plan plan) {
        validatePlan(plan);
        return dbService.updatePlan(plan);
    }

    public void removePlan(UUID id) {
        dbService.removePlan(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<Plan> getPlans(UUID customerId) {
        return dbService.getPlansByCustomer(customerId);
    }

    public List<Plan> getPlans() { return dbService.getAllPlans(); }
}
