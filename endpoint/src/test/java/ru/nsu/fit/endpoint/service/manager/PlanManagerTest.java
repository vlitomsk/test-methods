package ru.nsu.fit.endpoint.service.manager;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.Arrays;
import java.util.UUID;

public class PlanManagerTest {
    private DBService dbService;
    private Logger logger;
    private PlanManager planManager;

    private Plan planBeforeCreateMethod;
    private Plan planAfterCreateMethod;

    @Before
    public void before() {
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        planBeforeCreateMethod = new Plan()
                .setId(null)
                .setName("Lenin")
                .setDetails("honey")
                .setFee(1991);
        planAfterCreateMethod = planBeforeCreateMethod.clone();
        planAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createPlan(planBeforeCreateMethod)).thenReturn(planAfterCreateMethod);

        planManager = new PlanManager(dbService, logger);
    }

    @Test
    public void testCreateNewPlan() {
        Mockito.when(dbService.getPlanByName(planBeforeCreateMethod.getName())).thenReturn(null);
        Plan plan = planManager.createPlan(planBeforeCreateMethod);
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());
        Assert.assertEquals(2, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreatePlanWithWhitespaceDetails() {
        try {
            char[] content = new char[1000];
            Arrays.fill(content, ' ');
            planBeforeCreateMethod.setDetails(new String(content));
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Invalid plan details length (1..1024)", ex.getMessage());
        }
    }


    @Test
    public void testCreatePlanWithApostrhophe() {
        try {
            planBeforeCreateMethod.setDetails("hi hi hi '; drop table students; --");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertTrue(ex.getMessage().startsWith("Plan details contains invalid chars. Punctuation available:"));
        }
    }
}
