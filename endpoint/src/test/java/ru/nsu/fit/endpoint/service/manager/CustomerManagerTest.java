package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.UUID;

public class CustomerManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;

    private Customer customerBeforeUpdate;
    private Customer customerBeforeCreateMethod;
    private Customer customerAfterCreateMethod;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        customerBeforeCreateMethod = new Customer()
                .setId(null)
                .setFirstName("John")
                .setLastName("Wick")
                .setLogin("john_wick@gmail.com")
                .setPass("Baba_Jaga")
                .setBalance(0);
        customerAfterCreateMethod = customerBeforeCreateMethod.clone();
        customerAfterCreateMethod.setId(UUID.randomUUID());
        customerBeforeUpdate = customerAfterCreateMethod.clone();
        Mockito.when(dbService.getCustomerById(customerBeforeUpdate.getId())).thenReturn(customerBeforeUpdate);
        Mockito.when(dbService.createCustomer(customerBeforeCreateMethod)).thenReturn(customerAfterCreateMethod);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    public void testCreateNewCustomer() {
        Mockito.when(dbService.getCustomerByLogin(customerBeforeCreateMethod.getLogin())).thenReturn(null);
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
        Assert.assertEquals(customer, customerAfterCreateMethod);
        Assert.assertEquals(2, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateCustomerWithNullArgument() {
        try {
            customerManager.createCustomer(null);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'customerData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateCustomerWithExistingLogin() {
        try {
            customerManager.createCustomer(customerBeforeCreateMethod);
            Mockito.when(dbService.getCustomerIdByLogin(customerBeforeCreateMethod.getLogin())).thenReturn(customerAfterCreateMethod.getId());
            customerManager.createCustomer(customerBeforeCreateMethod);
            Assert.fail("Created two customers with same login");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Customer already exists", ex.getMessage());
        }
    }
//
    @Test
    public void testCreateCustomerWithEasyPassword() {
        try {
            customerBeforeCreateMethod.setPass("123qwe");
            customerManager.createCustomer(customerBeforeCreateMethod);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Password is easy.", ex.getMessage());
        }
    }

    @Test
    public void testUpdateCustomerMismatchLogin() {
        try {
            Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
            customer.setBalance(customer.getBalance() + 100);
            customer.setLogin("loh");
            customer.setPass("asfsddsdfsdf");
            customer.setFirstName("other name");
            Customer customerAfterUpdate = customerBeforeUpdate.clone();
            Mockito.when(dbService.updateCustomerNames(customerAfterCreateMethod)).thenReturn(customer);
            customerManager.updateCustomer(customer);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("mismatched customers (login)", ex.getMessage());
        }
    }

    @Test
    public void testUpdateCustomerMismatchBalance() {
        try {
            Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
            customer.setBalance(customer.getBalance() + 100);
            customer.setPass("asfsddsdfsdf");
            customer.setFirstName("other name");
            Customer customerAfterUpdate = customerBeforeUpdate.clone();
            Mockito.when(dbService.updateCustomerNames(customerAfterCreateMethod)).thenReturn(customer);
            customerManager.updateCustomer(customer);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("mismatched customers (balance)", ex.getMessage());
        }
    }

    @Test
    public void testUpdateCustomerMismatchPasswd() {
        try {
            Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
            customer.setPass("asfsddsdfsdf");
            customer.setFirstName("other name");
            Customer customerAfterUpdate = customerBeforeUpdate.clone();
            Mockito.when(dbService.updateCustomerNames(customerAfterCreateMethod)).thenReturn(customer);
            customerManager.updateCustomer(customer);
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("mismatched customers (passwd)", ex.getMessage());
        }
    }

    @Test
    public void testUpdateCustomerSuccess() {
        Customer customer = customerManager.createCustomer(customerBeforeCreateMethod);
        customer.setFirstName("other name");
        Customer customerAfterUpdate = customerBeforeUpdate.clone();
        Mockito.when(dbService.updateCustomerNames(customerAfterCreateMethod)).thenReturn(customer);
        Assert.assertEquals(customer, customerManager.updateCustomer(customer));
    }

    @Test
    public void testRemoveNullId() {
        try {
            customerManager.removeCustomer(null);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Id can't be null", e.getMessage());
        }
    }

}
